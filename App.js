import React, { Component } from 'react';
import { View, StyleSheet, Text, AsyncStorage, TextInput, TouchableOpacity, Platform } from 'react-native';

export default class Intermediario extends Component {

  constructor(props){
    super(props);
    this.state = {
      nome:'',
      nomeStorage:'',
      holder:'Digite seu nome'
    };

    AsyncStorage.getItem("nome").then((value) => {
      this.setState({nomeStorage:value});
    });

    this.setNome = this.setNome.bind(this);
    this.salvar = this.salvar.bind(this);
  }

  setNome(nome){
    let s = this.state;
    s.nome = nome;
    this.setState(s);
  }

  salvar(){
    let s = this.state;
    AsyncStorage.setItem("nome", s.nome);
    alert("Salvou");
    this.setState(s);
  }

  render() {

    return(

      <View style={styles.container}>
        <Text>{Platform.OS} {Platform.Version}</Text>
        <Text style={{ marginBottom:50, fontSize:30, fontWeight:'bold' }}>{this.state.nomeStorage}</Text>
        <TextInput style={styles.input} onChangeText={(texto) => this.setNome(texto)} placeholder={this.state.holder}></TextInput>
        <TouchableOpacity 
          style={{ 
            marginTop:10, 
            backgroundColor:'#0066CC', 
            height:40,
            width:100,
            justifyContent:'center',
            alignItems:'center',
            borderRadius:5 }}
          onPress={this.salvar}
          >
          <Text style={{ color:'#FFF' }}>Salvar</Text>
        </TouchableOpacity>
      </View>

    );
  }

}

const styles = StyleSheet.create({
  container:{
    flex:1,
    justifyContent:'center',
    alignItems:'center'
  },
  input:{
    height:40,
    width:200,
    borderWidth:0.3,
    borderColor:'black',
    fontSize:18,
    borderRadius:5
  }
});